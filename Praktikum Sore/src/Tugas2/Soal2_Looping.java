//Dudi Iskandar
//191106041405
// Program Menampilkan angka 10 angka genap pertama dengan for

package Tugas2;

public class Soal2_Looping {

	public static void main(String[] args) {
		
		for(int i = 2; i <= 20; i += 2){
            System.out.print( i + " ");
        }
	}
}
