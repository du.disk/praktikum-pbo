// Dudi Iskandar
// 191106041405
// Program Kasir

package Tugas2;

import java.util.Scanner;

public class Soal2_Kasir {

	public static void main(String[] args) {
		
		System.out.println("Program Kasir Sederhana");
		System.out.println("Dudi Iskandar");
		System.out.println("191106041405\n");
		
		Scanner keyboard = new Scanner(System.in);
		float TotalHarga=0, TotalBayar=0;
		int IndexMenu=0;
		String DaftarMenu[] = {"Nasi Goreng","Nasi Goreng Spesial","Nasi Putih","Ayam Bakar","Bebek Goreng","Sambal","Lalapan","Es Teh Manis","Es Jeruk","Air Putih"};
		float DaftarHarga[] = {15000, 25000, 8000, 15000, 20000, 5000, 7000, 5000, 7000, 0};
		
		System.out.println("*****-------------------------Sambalan Resto-------------------------*****");
        System.out.println("| *-DAFTAR MENU-* |");
        
        for (int i = 0; i < DaftarMenu.length; i++) {
			System.out.println(i+1 +". "+ DaftarMenu[i] +"........................................ Rp. "+ DaftarHarga[i]);
		}
        
        for (String Beli = "y"; Beli.equals("y") || Beli.equals("Y");)
        {
        	System.out.println();
            System.out.print("Siahkan Pilih Nomor Menu : ");
            IndexMenu = keyboard.nextInt();
            
            if(IndexMenu < DaftarMenu.length) {
            	System.out.println("Menu Yang Anda Pilih "+ DaftarMenu[IndexMenu-1]+ " Rp. "+ DaftarHarga[IndexMenu-1]);
            	TotalHarga +=DaftarHarga[IndexMenu-1];
            	
            	System.out.println();
                System.out.print("Apakah ingin memilih menu lagi? Y/T : ");
                Beli = keyboard.next();
            }else {
            	System.out.println();
                System.out.println("Menu yang anda pilih tidak tersedia, Apa anda ingin memilih menu lagi? Y/T");
                Beli = keyboard.next();
            }
        }
        
        System.out.println();
        System.out.println("Total yang harus di bayar : Rp. " + TotalHarga);
        
        System.out.print("Bayar : Rp. ");
        TotalBayar = keyboard.nextFloat();
        
        while(TotalBayar <= TotalHarga) {
        	System.out.println("Pembayaran Kurang dari Rp. "+ TotalHarga +" Silahkan Masukan Pembayaran dengan benar ");
        	System.out.print("Bayar : Rp. ");
            TotalBayar = keyboard.nextFloat();
        }
        
        System.out.println("Kembaliannya : Rp. " + (TotalBayar - TotalHarga));
        System.out.println("TERIMA KASIH TELAH MEMESAN");
        System.out.println("===============================================================================");
		
        keyboard.close();
	}

}


