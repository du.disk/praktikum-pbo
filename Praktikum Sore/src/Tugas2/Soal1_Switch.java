package Tugas2;

import java.util.Scanner;

public class Soal1_Switch {

	public static void main(String[] args) {
		int Semester, JumlahMatkul=0;
		String NamaMahasiswa, PesanError="", SPP="0";
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("=== Selamat Datang ===\n");
		
		System.out.print("Masukan Nama Anda : ");
		NamaMahasiswa = keyboard.nextLine();
		
		System.out.print("Masukan Semester  : ");
		Semester = Integer.parseInt(keyboard.nextLine());
		
		switch (Semester) {
		case 1: {
			SPP = "1.750.000";
			JumlahMatkul=9;
			break;
		}
		case 2: {
			SPP = "1.750.000";
			JumlahMatkul=8;
			break;
		}
		case 3: {
			SPP = "1.750.000";
			JumlahMatkul=8;
			break;
		}
		case 4: {
			SPP = "1.750.000";
			JumlahMatkul=8;
			break;
		}
		case 5: {
			SPP = "1.750.000";
			JumlahMatkul=9;
			break;
		}
		case 6: {
			SPP = "1.750.000";
			JumlahMatkul=8;
			break;
		}
		case 7: {
			SPP = "1.500.000";
			JumlahMatkul=6;
			break;
		}
		case 8: {
			SPP = "1.050.000";
			JumlahMatkul=2;
			break;
		}
		default:
			PesanError="Cepat-cepatlah Lulus :)";
			break;
		}
		
		keyboard.close();
		System.out.println("\n============================================================");
		if(PesanError !="") {
			System.out.println(PesanError);
		}else {
			System.out.println("Nama                    : " + NamaMahasiswa);
			System.out.println("Semester                : " + Semester);
			System.out.println("SPP                     : RP." + SPP);
			System.out.println("Jumlah Matakuliah Wajib : " + JumlahMatkul);
		}
		System.out.println("============================================================");
	}

}


