//Dudi Iskandar
//191106041405
// Program menampilkan semua bilangan kelipatan 5, mulai dari angka 5. Menggunakan modulus untuk mencari kelipatan 5 (selama bilangan < 100)

package Tugas2;

public class Soal2_looping_2 {

	public static void main(String[] args) {

		int loop=1;
		while(loop < 100) {
			if(loop %5==0 ) {
				System.out.print(loop + " ");
			}
			loop++;
		}

	}

}
