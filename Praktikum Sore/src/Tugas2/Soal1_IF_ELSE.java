package Tugas2;

import java.util.Scanner;

public class Soal1_IF_ELSE {

	public static void main(String[] args) {
		float BeratBadan, TinggiBadan;
		String Olahraga ="Yang Cocok dengan anda yaitu ";
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("=== Selamat Datang di Aplikasi Pencari Olahraga sesuai dengan berat dan tinggi badan ===\n");
		
		System.out.print("Masukan Tinggi Badan Anda : ");
		TinggiBadan = keyboard.nextInt();
		
		System.out.print("Masukan Berat Badan Anda : ");
		BeratBadan = keyboard.nextInt();
		
		if(TinggiBadan >= 170) {
			if(BeratBadan >= 70) {
				Olahraga += "berolahraga lari atau bersepeda";
			}else{
				Olahraga += "berolahraga Latihan Beban";
			}
		}else{
			if(BeratBadan >= 70) {
				Olahraga += "berolahraga Renang";
			}else {
				Olahraga += "berolahraga Aerobik";
			}
		}
		
		keyboard.close();
		System.out.println("\n============================================================");
		System.out.println(Olahraga);
		System.out.println("============================================================");
	}

}


