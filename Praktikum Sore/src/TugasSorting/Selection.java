package TugasSorting;

import java.util.Vector;

public class Selection {
    public static Vector<Float> Sort(Vector<Float> vector) {
	    int i;
	    float ukuran = Data.getUkuranVector();
	    int max;
	    while (ukuran > 0) {
	        max = 0;
	        for (i = 1; i < ukuran; i++) {
	            if (vector.get(max) < vector.get(i)) {
	                max = i;
                }
            }
	        Float temp = vector.get(max);
	        vector.set(max, vector.get((int) (ukuran - 1)));
	        vector.set((int) (ukuran - 1), temp);
	        ukuran--;
        }
    	return vector;
    }
}

