package TugasSorting;

import java.util.Vector;

public class Bubble {
	static Vector<Float> vectorBubble;
	public static Vector<Float> Sort(Vector<Float> vector) {
		int n = 0;
		float ukuran = Data.getUkuranVector();
		while (n < ukuran) {
			for (int i = 1; i < ukuran; i++) {
				if (vector.get(i - 1) > vector.get(i)) {
					Float temp = vector.get(i);
					vector.set(i, vector.get(i - 1));
					vector.set(i - 1, temp);
				}
			}
			n++;
		}
		return vector;
	}
}

