package TugasSorting;

import java.util.Vector;

public class Shell {
    static float N, distance;
    static float j, i;
    static Vector<Integer> vectorShell;
    public static Vector<Float> shellSort(Vector<Float> vector) {
	    N = Data.getUkuranVector();
	    distance = N / 2;
	    while (distance > 0) {
	        for (i = 0; i < N - distance; i++) {
	            j = i + distance;
	            if (vector.get((int) i) > vector.get((int) j)) {
	                Float temp = vector.get((int) i);
	                vector.set((int) i, vector.get((int) j));
	                vector.set((int) j, temp);
	            }
	        }
	        distance = distance / 2;
	    }
	    return vector;
    }
}