package TugasSorting;

import java.util.Vector;

public class Insertion {
    public static Vector<Float> Sort(Vector<Float> vector) {
	    int i = 1;
	    int index;
	    float ukuran = Data.getUkuranVector();
	    while (i < ukuran) {
	        Float temp = vector.get(i);
	        for (index = i; index > 0; index--) {
	            if (temp < vector.get(index - 1)) {
	                vector.set(index, vector.get(index - 1));
	            }
	            else break;
	        }
	        vector.set(index, temp);
	        i++;
	    }
	    return vector;
	}
}

