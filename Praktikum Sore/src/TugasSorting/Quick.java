package TugasSorting;

import java.util.Vector;

public class Quick {
    private static void swap(Vector<Float> vector, int left,float right) {
	    Float temp = vector.get(left);
	    vector.set(left, vector.get((int) right));
	    vector.set((int) right, temp);
	}
    
	public static Vector < Float > quickSort(Vector<Float> vector) {
		float ukuran = Data.getUkuranVector() -1;
	    quickSortRecursive(vector, 0, ukuran);
	    return vector;
	}
	
	private static void quickSortRecursive(Vector<Float> vector, int left, float right) {
	    float pivot;
	    if (left >= right)
	        return;
	    pivot = partition(vector, left, right);
	    quickSortRecursive(vector, left, pivot - 1);
	    quickSortRecursive(vector, (int) (pivot + 1), right);
	}
	
	public static float partition(Vector<Float> vector, float left, float right) {
	    while (true) {
	        while ((left < right) && (vector.get((int) left) < vector.get((int) right)))
	            right--;
	        if (left < right) swap(vector, (int) left, right);
	        else return left;
	        while ((left < right) && (vector.get((int) left) < vector.get((int) right)))
	            left++;
	        if (left < right) swap(vector, (int) left, right--);
	        else return right;
	    }
	}
}