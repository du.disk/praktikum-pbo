// Dudi Iskandar
// 191106041405
// Program Sederhana untuk membuat laporan dengan array

package TugasArray;

import java.util.Scanner;

public class Tugas {

	public static void main(String[] args) {
		System.out.println("Program Sederhana Untuk Membuat Laporan");
		System.out.println("Dudi Iskandar");
		System.out.println("191106041405\n");
		
		Scanner keyboard = new Scanner(System.in);
		String Nama[] = new String[100];
		float TB[] = new float[100];;
		float BB[] = new float[100];
		int IndexData = 0;
		
		for (String InputKembali = "y"; InputKembali.equals("y") || InputKembali.equals("Y");){
			
			System.out.print("Masukan Nama : " );
            Nama[IndexData] = keyboard.next();
            
            System.out.print("Masukan Tinggi Badan : ");
            TB[IndexData] = keyboard.nextFloat();
            
            System.out.print("Masukan Berat Badan : ");
            BB[IndexData] = keyboard.nextFloat();
			
			System.out.println();
            System.out.print("Apakah mau kembali input data? Y/T : ");
            InputKembali = keyboard.next();
            IndexData++;
        }
		
		System.out.println("-----------------------------------------------------------------------------");
		System.out.format("| %3s", "No");
    	System.out.format("| %15s", "Nama Mahasiswa");
    	System.out.format("| %15s", "Tinggi Badan (cm)");
    	System.out.format("| %15s |", "Berat Badan (kg)");
    	System.out.println();
    	System.out.println("-----------------------------------------------------------------------------");
        for(int i = 0; i < IndexData; i++){
        	System.out.format("| %3s ", i+1);
        	System.out.format("| %15s", Nama[i]);
        	System.out.format("| %15s", TB[i]);
        	System.out.format("| %15s |", BB[i]);
            System.out.println("");
        }
        System.out.println("-----------------------------------------------------------------------------");
        keyboard.close();
	}

}
