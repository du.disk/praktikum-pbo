//Nama Dudi Iskandar
//Kelas Karyawan A
//Semester 4 - Teknik Informatika

package cobaTipeData;

//mengimpor_Scanner_ke_program
import java.util.Scanner;

public class Tugas1 {

	public static void main(String[] args) {
		//Deklarasi_Variable
		String JenisKonversi, Data;
		
		//Buat_Scanner_Baru
		Scanner keyboard = new Scanner(System.in);
		
		//Pilih_Jenis_Konversi
		System.out.println("\n=== Tipe Data yang tersedia byte, integer, long, float, double, char, boolean, dan string ===\n");
		
		System.out.print("Masukan data yang akan di konversi: ");
		Data = keyboard.nextLine();
		
		System.out.print("Konversi ke : ");
		JenisKonversi = keyboard.nextLine();
		
		//Tutup_Keyboard
		keyboard.close();
		
		try {
			//Deklarasi_Variable_Hasil
			String hasil="";
			//Konversi_Data_String_Ke_float
			float stringToFloat = Float.parseFloat(Data);
			
			//Switch_untuk_kondisi_Jenis_Konversi
			switch (JenisKonversi) {
				case "byte": {
					//Konversi_dari_Float_ke_Byte
					//dan_dari_byte_ke_String
					hasil = String.valueOf((byte) stringToFloat);
					break;
				}
				case "integer": {
					//pembulatan_hasil_konversi_float
					stringToFloat = Math.round(stringToFloat);
					//Konversi_dari_Pembulatan_Float_ke_Integer
					//dan_dari_integer_ke_String
					hasil = String.valueOf((int) stringToFloat);
					break;
				}
				case "long": {
					//Konversi_dari_Float_ke_Long
					//dan_dari_byte_ke_String
					hasil = String.valueOf((long) stringToFloat);
					break;
				}
				case "float": {
					//dan_dari_float_ke_String
					hasil = String.valueOf(stringToFloat);
					break;
				}
				case "double": {
					//Konversi_dari_Float_ke_double
					//dan_dari_double_ke_String
					hasil = String.valueOf((double) stringToFloat);
					break;
				}
				case "char": {
					//Konversi_dari_Float_ke_Char
					//dan_dari_char_ke_String
					hasil = String.valueOf((char) stringToFloat);
					break;
				}
				case "boolean": {
					//Konversi_data_yang_data_lebih_dari_0_menjadi_true_dan_di_bawah_0_menjadi_false
					boolean bool=stringToFloat>0?true:false;
					//dan_dari_boolean_ke_String
					hasil = String.valueOf(bool);
					break;
				}
				case "string": {
					hasil = Data;
					break;
				}
				default:{
					break;
				}
			}
			
			System.out.println("\n=========================================");
			//Cetak_output
			if(hasil !="") {
				System.out.println("\nHasil dari konversi Data : "+ Data +" ke " + JenisKonversi +" adalah " + hasil);
			}else {
				System.out.println("\nTidak ada pilihan konversi data ke : " + JenisKonversi);
			}
			
			System.out.println("\n=========================================");
			
		} catch (Exception e) {
			//Handle_jika_terdapat_error
			System.out.println("\n=========================================");
			System.out.println("\nError saat konversi " + e.getMessage());
			System.out.println("\n=========================================");
		}
	}

}
