//Nama Dudi Iskandar
//Kelas Karyawan A
//Semester 4 - Teknik Informatika
//Program Menghitung Pajak Pendapatan

package cobaTipeData;

//mengimpor_Scanner_ke_program
import java.util.Scanner;

public class TaxCalculate {

	public static void main(String[] args) {
		Employee employee = new Employee();
		
		//Buat_Scanner_Baru
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("====================================================");
		System.out.println("===== Welcome to Tax Calculate with PPh " + (int)(employee.getPPH()*100) + "% =====");
		System.out.println("====================================================");
		
		System.out.print("Enter Full Name : ");
		employee.setName(keyboard.nextLine());
		
		System.out.print("Enter Age : ");
		employee.setAge(keyboard.nextLine());
		
		System.out.print("Marital Status (yes/no) : ");
		employee.setMaritalStatus(keyboard.nextLine());
		
		if(employee.isMaritalStatus()) {
			System.out.print("How many childern you have : ");
			employee.setChildern(keyboard.nextLine());
		}
		
		System.out.print("Base Salary : ");
		employee.setBaseSalary(keyboard.nextLine());
		
		employee.setPTKP(employee.isMaritalStatus(), employee.getChildern());
		
		keyboard.close();
		
		System.out.println("====================================================");
		System.out.println("====================== Result ======================");
		System.out.println("====================================================");
		
		System.out.println("Full Name		: " + employee.getName());
		System.out.println("Age 			: " + employee.getAge());
		System.out.println("Marital Status		: " + employee.isMaritalStatus());
		if(employee.isMaritalStatus()) {
			System.out.println("Childern			: " + employee.getChildern());
		}
		
		
		System.out.println("Base Salary		: " + String.valueOf((long) employee.getBaseSalary()));
		System.out.println("PTKP			: " + String.valueOf((long) employee.getPTKP()));
		System.out.println("PPh			: " + (int)(employee.getPPH()*100) +"%");
		System.out.println("Yearly Tax		: " + String.valueOf((long) employee.getTax(employee.getPTKP(),employee.getBaseSalary())));
		System.out.println("Monthly Tax		: " + String.valueOf((long) employee.getTax(employee.getPTKP(),employee.getBaseSalary())/12));
		
		System.out.println("====================================================");
		System.out.println("====================== End Result ==================");
		System.out.println("====================================================");
	}

}
