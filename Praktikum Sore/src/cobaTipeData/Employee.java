//Nama Dudi Iskandar
//Kelas Karyawan A
//Semester 4 - Teknik Informatika
//Program Menghitung Pajak Pendapatan

package cobaTipeData;

public class Employee {
	String Name;
	int Age, Childern;
	boolean MaritalStatus;
	double BaseSalary, PTKP=54000000;
	double PPH=0.15;
	
	//Getter Setter Name
	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	//Getter Setter Age
	public int getAge() {
		return Age;
	}
	
	public void setAge(String age) {
		Age = Integer.parseInt(age);
	}
	
	//Getter Setter Childern
	public int getChildern() {
		return Childern;
	}
	
	public void setChildern(String childern) {
		Childern = Integer.parseInt(childern);
	}
	
	//Getter Setter Base Salary
	public double getBaseSalary() {
		return BaseSalary;
	}
	
	public void setBaseSalary(String baseSalary) {
		BaseSalary = Double.parseDouble(baseSalary);
	}
	
	//Getter Setter Marital Status
	public boolean isMaritalStatus() {
		return MaritalStatus;
	}
	
	public void setMaritalStatus(String maritalStatus) {
		if(maritalStatus=="yes")
			MaritalStatus = true;
		else
			MaritalStatus=false;
	}
	
	//Getter Setter PPH
	public double getPPH() {
		return PPH;
	}

	public void setPPH(double pPH) {
		PPH = pPH;
	}
	
	//Getter Setter PTKP
	public double getPTKP() {
		return PTKP;
	}

	public void setPTKP(boolean maritalStatus, int childern) {
		if(BaseSalary > 3500000) {
			if(maritalStatus) {
				double addtional=4500000;
				PTKP += addtional;
				for (int i = 0; i < childern; i++) {
					PTKP +=addtional;
				}
			}
		}else {
			PTKP = 0;
		}
	}
	
	public double getTax(double PTKP, double baseSalary) {
		double tax=0;
		
		double yearlySalary = baseSalary * 12;
		double netIncome = yearlySalary - PTKP;
		
		tax = PPH * netIncome;
		return tax;
	}
	
	
}
