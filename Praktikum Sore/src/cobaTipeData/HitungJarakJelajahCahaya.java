package cobaTipeData;

public class HitungJarakJelajahCahaya {

	public static void main(String[] args) {
		int kecCahaya;
		long hari;
		long detik;
		long jarak;
		
		//Kecepatan_cahaya_dalam_km_per_detik_(pendekatan)
		kecCahaya = 340000;
		hari = 1000; //Menetapkan_jumlah_hari
		detik = hari *24 * 60 * 60; //Mengkonversi_ke_detik
		jarak = kecCahaya * detik; //Menghitung_detik
		
		System.out.print("Dalam "+hari);
		System.out.print(" hari, cahaya menjelajah berkisar ");
		System.out.println(jarak + " km");

	}

}
