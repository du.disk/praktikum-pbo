package cobaTipeData;

public class DemoTipeDataBoolean {

	public static void main(String[] args) {
		boolean b;
		
		b = false;
		System.out.println("b adalah " + b);
		
		b = true;
		System.out.println("b adalah " + b);
		
		//Suatu_nilai_boolean_dapat_mengendalikan_statement_if
		if(b) System.out.println("Ini dieksekusi.");
		
		b = false;
		if(b) System.out.println("Ini tidak dieksekusi.");
		
		System.out.println("10 > 9 adalah " + (10 > 9));

	}

}
