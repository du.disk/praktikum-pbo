package ProgramUTS;

import java.util.Scanner;

public class BilanganKelipatan3 {

	public static void main(String[] args) {
		// Header Berisi nama dan NPM
		System.out.println("Program Kelipatan 3");
		System.out.println("Dudi Iskandar");
		System.out.println("191106041405\n");
			
		//Deklarasi Variable
		Scanner keyboard = new Scanner(System.in);
		int angka=0;
		
		System.out.print("Berapa angka kelipatan 3 yang diinginkan : ");
		angka = keyboard.nextInt();

		System.out.println("Output : \n");
		int nilai_awal=3;
		for (int i = 0; i < angka; i++) {
				
			System.out.print(nilai_awal);
			nilai_awal += 3;
			int nilaiplus1 = i+1;
			 
			 if(i != angka-1 && (nilaiplus1 %6 !=0)) {
				 System.out.print(", ");
			 }else if(nilaiplus1 %6 ==0) {
				 System.out.println("");
			 }
		}
	}

}
