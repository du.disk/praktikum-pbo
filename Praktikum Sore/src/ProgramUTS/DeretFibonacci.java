package ProgramUTS;

import java.util.Scanner;

public class DeretFibonacci {
	// Fungsi Fibonaccii
	static int fibonacci(int angka){
	    if(angka == 0 || angka == 1){
	      return angka;
	    } else {
	      return (fibonacci(angka-1) + fibonacci(angka-2));
	    }
	  }
	
	public static void main(String[] args) {
		// Header Berisi nama dan NPM
		System.out.println("Program Fibonacci");
		System.out.println("Dudi Iskandar");
		System.out.println("191106041405\n");
		
		//Deklarasi Variable
		Scanner keyboard = new Scanner(System.in);
		int angka=0;
		
		System.out.print("Berapa angka Fibonacci yang diinginkan ? ");
		angka = keyboard.nextInt();
		
		System.out.println("Output : \n");
		
		for (int i = 0; i < angka; i++) {
			
			 System.out.print(fibonacci(i));
			 
			 int nilaiplus1 = i+1;
			 
			 if(i != angka-1 && (nilaiplus1 %5 !=0)) {
				 System.out.print(", ");
			 }else if(nilaiplus1 %5 ==0) {
				 System.out.println("");
			 }
		}
		
        keyboard.close();
	}

}
