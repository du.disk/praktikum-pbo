//@Author Dudi Iskandar
//May 31, 2021 20:00
//Super Class Segi Empat

package Pewarisan;

public class SegiEmpat {
	
	protected int luas() {
		System.out.println("Menghitung Luas dari bentuk-bentuk Segi Empat");
		return 0;
	}
	
	protected int keliling() {
		System.out.println("Menghitung keliling dari bentuk-bentuk Segi Empat \n");
		return 0;
	}
}
