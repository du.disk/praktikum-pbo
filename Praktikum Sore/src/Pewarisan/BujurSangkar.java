//@Author Dudi Iskandar
//May 31, 2021 20:00
//Sub Class Bujur Sangkar

package Pewarisan;

public class BujurSangkar extends SegiEmpat{
	
	int sisi=0;
	
	protected int luas() {
		int luas = sisi * sisi;
		System.out.println("Luas Bujur Sangkar dari sisi "+sisi+" adalah "+luas);
		return luas;
	}
	
	protected int keliling() {
		int keliling = 4 * sisi;
		System.out.println("Keliling Bujur Sangkar dari sisi "+sisi+" adalah "+keliling+" \n");
		return keliling;
	}
}
