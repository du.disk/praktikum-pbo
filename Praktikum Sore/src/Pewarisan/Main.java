//@Author Dudi Iskandar
//May 31, 2021 20:00
//Main Class

package Pewarisan;

public class Main {

	public static void main(String[] args) {
		
		//Membuat Object Class
		SegiEmpat segiEmpat = new SegiEmpat();
		PersegiPanjang persegiPanjang = new PersegiPanjang();
		BujurSangkar bujurSangkar = new BujurSangkar();
		Trapesium trapesium = new Trapesium();
		
		//Memberikan nilai pada setiap properti dalam object
		persegiPanjang.panjang=10;
		persegiPanjang.lebar=7;
		
		bujurSangkar.sisi=8;
		
		trapesium.alas_a=8;
		trapesium.alas_b=10;
		trapesium.sisi_a=8;
		trapesium.sisi_b=7;
		trapesium.sisi_c=5;
		trapesium.sisi_d=10;
		
		//Menampilkan Luas dan keliling pada setiap object
		segiEmpat.luas();
		segiEmpat.keliling();
		
		persegiPanjang.luas();
		persegiPanjang.keliling();
		
		bujurSangkar.luas();
		bujurSangkar.keliling();
		
		trapesium.luas();
		trapesium.keliling();
		
	}

}
