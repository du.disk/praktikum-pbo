//@Author Dudi Iskandar
//May 31, 2021 20:00
//Sub Class Trapesium

package Pewarisan;

public class Trapesium extends SegiEmpat{
	
	int tinggi=0;
	
	int sisi_a=0;
	int sisi_b=0;
	int sisi_c=0;
	int sisi_d=0;
	
	int alas_a=0;
	int alas_b=0;
	
	protected int luas() {
		int luas = ((alas_a + alas_b) * tinggi)/2;
		System.out.println("Luas Trapesium dari Alas a "+alas_a+", Alas b "+alas_b+" dan tinggi "+tinggi+"  adalah "+luas);
		return luas;
	}
	
	protected int keliling() {
		int keliling = sisi_a + sisi_b + sisi_c + sisi_d;
		System.out.println("Keliling Trapesium dari sisi a "+sisi_a+", sisi b "+sisi_b+", sisi c "+sisi_c+" dan sisi d "+sisi_d+" adalah "+keliling+" \n");
		return keliling;
	}
}
