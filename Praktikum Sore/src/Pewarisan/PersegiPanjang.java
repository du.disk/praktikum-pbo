//@Author Dudi Iskandar
//May 31, 2021 20:00
//Sub Class Persegi Panjang

package Pewarisan;

public class PersegiPanjang extends SegiEmpat{
	
	int panjang=0;
	int lebar=0;
	
	protected int luas() {
		int luas = panjang * lebar;
		System.out.println("Luas Persegi Panjang dari panjang "+panjang+" dan lebar "+lebar+" adalah "+luas);
		return luas;
	}
	
	protected int keliling() {
		int keliling = 2 * (panjang + lebar);
		System.out.println("Keliling Persegi Panjang dari panjang "+panjang+" dan lebar "+lebar+" adalah "+keliling+" \n");
		return keliling;
	}
}
