package mobil_3;

public class TestDrive3 {

	public static void main(String[] args) {
		Mobil mobilbaru = new Mobil();
		
		mobilbaru.setWarna("Biru");
		mobilbaru.setJumlah_Pintu(2);
		mobilbaru.setIsi_Tangki(25);
		
		System.out.println
		("Mobil baru baruku berwarna " + mobilbaru.getWarna() + 
		" dengan " + mobilbaru.getJumlah_Pintu() + 
		" pintu dan dapat menampung bahan bakar sebanyak " + mobilbaru.getIsi_Tangki() + " liter");

	}

}
