package mobil_3;

public class Mobil {
	private String Warna;
	private int Jumlah_Pintu;
	private double Isi_Tangki;
	
	//Getter_Setter_Warna
	public String getWarna() {
		return Warna;
	}
	public void setWarna(String warna) {
		Warna = warna;
	}
	
	//Getter_Setter_Jumlah_Pintu
	public int getJumlah_Pintu() {
		return Jumlah_Pintu;
	}
	public void setJumlah_Pintu(int jumlah_Pintu) {
		Jumlah_Pintu = jumlah_Pintu;
	}
	
	//Getter_Setter_Isi_Tangki
	public double getIsi_Tangki() {
		return Isi_Tangki;
	}
	public void setIsi_Tangki(double isi_Tangki) {
		Isi_Tangki = isi_Tangki;
	}
	
	
}
